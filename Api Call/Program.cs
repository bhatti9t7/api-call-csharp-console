﻿using Api_Call.Serialization;
using Api_Call.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Api_Call
{

    class Program
    {
        static void Main(string[] args)
        {

            PostSerialization postSerialization = new PostSerialization();


            #region Fetch List of Posts
            IRestResponse response = Helper.executeRequest("posts", Method.GET, null);
            List<Post> posts = postSerialization.DeserializeList(response);
            if (posts != null)
            {
                Console.WriteLine("Post fethed: " + posts.Count);
            }
            else
            {
                Console.WriteLine("Error Occured white fething posts");
            }
            #endregion



            #region Fetch Single Post
            IRestResponse responseSingle = Helper.executeRequest("posts/1", Method.GET, null);
            Post post = postSerialization.DeserializeSingle(responseSingle);
            if (post != null)
            {
                Console.WriteLine("Post Title: " + post.title);
            }
            else
            {
                Console.WriteLine("Error Occured white fething post");
            }
            #endregion



            #region Add Post
            Post newPost = new Post();
            newPost.title = "How to do C# code";
            newPost.body = "Here is body";
            newPost.userId = 1;
            IRestResponse responseAddedPost = Helper.executeRequest("posts", Method.POST, postSerialization.SerializeObject(newPost));
            Post postAdded = postSerialization.DeserializeSingle(responseAddedPost);
            if (postAdded != null)
            {
                Console.WriteLine("Added Post Title: " + postAdded.title);
            }
            else
            {
                Console.WriteLine("Error Occured while updating post");
            }
            #endregion


            #region Update Post
            post.title += " - changed";             //Add change in title to view changes
            IRestResponse responseUpdatedPost = Helper.executeRequest("posts/" + post.id, Method.PUT, postSerialization.SerializeObject(post));
            Post postUpdated = postSerialization.DeserializeSingle(responseUpdatedPost);
            if (postUpdated != null)
            {
                Console.WriteLine("Updated Post Title: " + postUpdated.title);
            }
            else
            {
                Console.WriteLine("Error Occured while updating post");
            }
            #endregion


            #region Delete Post
            IRestResponse responseDeletedPost = Helper.executeRequest("posts/" + post.id, Method.DELETE, null);
            Post postDeleted = postSerialization.DeserializeSingle(responseDeletedPost);
            Console.WriteLine(postDeleted.title == null ? "Deleted id: " + post.id : "Unable to Delete");
            #endregion



            Console.ReadLine();     //for show results
        }
    }   
    
}
