﻿using Api_Call.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Api_Call.Serialization
{
    class PostSerialization
    {
        JavaScriptSerializer ser;
        public PostSerialization()
        {
            ser = new JavaScriptSerializer();
        }
        public List<Post> DeserializeList(IRestResponse response)
        {
            try
            {
                return ser.Deserialize<List<Post>>(response.Content);
            }
            catch (Exception)
            {
               return null;
            }
                
                
        }
        public Post DeserializeSingle(IRestResponse response)
        {
            try
            {
                return JsonConvert.DeserializeObject<Post>(response.Content);
            }
            catch (Exception)
            {
                return null;
            }
             
        }
        public string SerializeObject(Post post)
        {
            return new JavaScriptSerializer().Serialize(post);
        }
    }
}
