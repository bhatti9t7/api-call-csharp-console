﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api_Call
{
    static class Helper
    {
        public static IRestResponse executeRequest(string action, Method method,string paramss)
        {
            var client = new RestClient("https://jsonplaceholder.typicode.com/"+action);
            var request = new RestRequest(method);
            request.AddHeader("postman-token", "53a3efda-2024-1f04-12e2-1d68f9e8b97a");
            request.AddHeader("cache-control", "no-cache");
            if (paramss != null && paramss.Length > 0)
            {
                request.AddParameter("application/json", paramss,ParameterType.RequestBody);
            }
            IRestResponse response = client.Execute(request);
            return response;
        }
        

    }
}
